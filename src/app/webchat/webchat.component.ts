import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Subject} from 'rxjs';
import {Observable} from 'rxjs';
import { AngularAgoraRtcService, Stream } from 'angular-agora-rtc';


@Component({
  selector: 'app-webchat',
  templateUrl: './webchat.component.html',
  styleUrls: ['./webchat.component.css']
})
export class WebchatComponent implements OnInit {

  title = 'AgoraDemo';
  localStream: Stream; // Add
  remoteCalls: any = [];
  showVideo = true;
  uid = 1;
  mute = false;

  // Add
  constructor(
    private agoraService: AngularAgoraRtcService
  ) {
    // this.agoraService.createClient();
    // console.log(this.agoraService.client);
  }

  ngOnInit() {}

  // Add
  startCall() {
    this.agoraService.createClient();
    this.agoraService.client.join(null, '1001', null, (uid) => {
      console.log(this.uid);
      this.localStream = this.agoraService.createStream(this.uid, true, null, null, true, false);
      this.localStream.setVideoProfile('720p_3');
      console.log(this.localStream);
      this.subscribeToStreams();
    });
  }

  // Add
  private subscribeToStreams() {
    this.localStream.on("accessAllowed", (r) => {
      console.log(r);
      console.log("accessAllowed");
    });
    // The user has denied access to the camera and mic.
    this.localStream.on("accessDenied", () => {
      console.log("accessDenied");
    });

    this.localStream.init(() => {
      console.log("getUserMedia successfully");
      this.localStream.play('agora_local');
      console.log(this.localStream);
      this.agoraService.client.publish(this.localStream, (err) => {
        console.log("Publish local stream error: " + err);
      });
      this.agoraService.client.on('stream-published', (evt) => {
        console.log(evt, "Publish local stream successfully");
      });
    }, (err) => {
      console.log("getUserMedia failed", err);
    });

    // Add
    this.agoraService.client.on('error', (err) => {
      console.log("Got error msg:", err.reason);
      if (err.reason === 'DYNAMIC_KEY_TIMEOUT') {
        this.agoraService.client.renewChannelKey("", (r) => {
          console.log(r, "Renew channel key successfully");
        }, (err) => {
          console.log("Renew channel key failed: ", err);
        });
      }
    });

    // Add
    this.agoraService.client.on('stream-added', (evt) => {
      const stream = evt.stream;
      console.log(evt.stream);
      this.agoraService.client.subscribe(stream, (err) => {
        console.log("Subscribe stream failed", err);
      });
    });

    // Add
    this.agoraService.client.on('stream-subscribed', (evt) => {
      const stream = evt.stream;
      if (!this.remoteCalls.includes(`agora_remote${stream.getId()}`)) this.remoteCalls.push(`agora_remote${stream.getId()}`);
      setTimeout(() => stream.play(`agora_remote${stream.getId()}`), 2000);
    });

    // Add
    this.agoraService.client.on('stream-removed', (evt) => {
      const stream = evt.stream;
      stream.stop();
      this.remoteCalls = this.remoteCalls.filter(call => call !== `#agora_remote${stream.getId()}`);
      console.log(`Remote stream is removed ${stream.getId()}`);
    });

    // Add
    this.agoraService.client.on('peer-leave', (evt) => {
      const stream = evt.stream;
      if (stream) {
        stream.stop();
        this.remoteCalls = this.remoteCalls.filter(call => call === `#agora_remote${stream.getId()}`);
        console.log(`${evt.uid} left from this channel`);
      }
    });
  }

  closeVideo() {
    this.localStream.stop();
    this.localStream.close();
    console.log(this.agoraService.client.gatewayClient);
    console.log(this.agoraService.client.gatewayClient);
    this.agoraService.client.stopLiveStreaming(this.agoraService.client.gatewayClient);
    this.agoraService.client.unpublish(this.agoraService.client.gatewayClient);
    // https://webchat101-9fd8c.firebaseapp.com/
  }
  
  muteUnmute() {
    this.mute = !this.mute;
    if(this.mute) {
      this.localStream.disableAudio();
    } else {
      this.localStream.enableAudio();
    }
  }

  hideCaller() {}
}
